﻿namespace Dunno.PView
{
    partial class FLopHoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BTN_Them = new System.Windows.Forms.Button();
            this.RDB_NganHan = new System.Windows.Forms.RadioButton();
            this.RDB_DaiHan = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.TB_HocPhi = new System.Windows.Forms.TextBox();
            this.DTP_NgayKhaiGiang = new System.Windows.Forms.DateTimePicker();
            this.CBB_TKB = new System.Windows.Forms.ComboBox();
            this.TB_TenLopHOc = new System.Windows.Forms.TextBox();
            this.TB_MaLopHoc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.DT_MonHoc = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DT_LopHoc = new System.Windows.Forms.DataGridView();
            this.trungTamDunnoDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.trungTamDunnoDataSet = new Dunno.TrungTamDunnoDataSet();
            this.trungTamDunnoDataSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.quanLyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taiKhoanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinhVienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bienLaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quanLyTaiKhoanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dangXuatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DT_MonHoc)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DT_LopHoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSetBindingSource1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Location = new System.Drawing.Point(2, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1105, 238);
            this.panel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button9);
            this.panel6.Controls.Add(this.button8);
            this.panel6.Controls.Add(this.button7);
            this.panel6.Controls.Add(this.button1);
            this.panel6.Controls.Add(this.textBox7);
            this.panel6.Controls.Add(this.textBox6);
            this.panel6.Controls.Add(this.textBox5);
            this.panel6.Controls.Add(this.textBox4);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Location = new System.Drawing.Point(644, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(461, 232);
            this.panel6.TabIndex = 1;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(341, 113);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 12;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(250, 113);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 11;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(341, 75);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 10;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(250, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(128, 175);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 8;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(128, 142);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 7;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(128, 107);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 6;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(128, 76);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 14);
            this.label12.TabIndex = 4;
            this.label12.Text = "So Tiet Thuc Hanh";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 14);
            this.label11.TabIndex = 3;
            this.label11.Text = "So Tiet Ly Thuyet";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 14);
            this.label10.TabIndex = 2;
            this.label10.Text = "Ten Lop Hoc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 14);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ma Lop Hoc";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(179, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "QUAN LY MON HOC";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button6);
            this.panel5.Controls.Add(this.button5);
            this.panel5.Controls.Add(this.button4);
            this.panel5.Controls.Add(this.button3);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.BTN_Them);
            this.panel5.Controls.Add(this.RDB_NganHan);
            this.panel5.Controls.Add(this.RDB_DaiHan);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TB_HocPhi);
            this.panel5.Controls.Add(this.DTP_NgayKhaiGiang);
            this.panel5.Controls.Add(this.CBB_TKB);
            this.panel5.Controls.Add(this.TB_TenLopHOc);
            this.panel5.Controls.Add(this.TB_MaLopHoc);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.menuStrip1);
            this.panel5.Location = new System.Drawing.Point(0, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(638, 232);
            this.panel5.TabIndex = 0;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(522, 189);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 19;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(410, 189);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 18;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(522, 152);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(410, 152);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(522, 113);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // BTN_Them
            // 
            this.BTN_Them.Location = new System.Drawing.Point(410, 113);
            this.BTN_Them.Name = "BTN_Them";
            this.BTN_Them.Size = new System.Drawing.Size(75, 23);
            this.BTN_Them.TabIndex = 14;
            this.BTN_Them.Text = "Them";
            this.BTN_Them.UseVisualStyleBackColor = true;
            this.BTN_Them.Click += new System.EventHandler(this.BTN_Them_Click);
            // 
            // RDB_NganHan
            // 
            this.RDB_NganHan.AutoSize = true;
            this.RDB_NganHan.Location = new System.Drawing.Point(525, 65);
            this.RDB_NganHan.Name = "RDB_NganHan";
            this.RDB_NganHan.Size = new System.Drawing.Size(72, 18);
            this.RDB_NganHan.TabIndex = 13;
            this.RDB_NganHan.TabStop = true;
            this.RDB_NganHan.Text = "Ngan Han";
            this.RDB_NganHan.UseVisualStyleBackColor = true;
            // 
            // RDB_DaiHan
            // 
            this.RDB_DaiHan.AutoSize = true;
            this.RDB_DaiHan.Location = new System.Drawing.Point(457, 65);
            this.RDB_DaiHan.Name = "RDB_DaiHan";
            this.RDB_DaiHan.Size = new System.Drawing.Size(62, 18);
            this.RDB_DaiHan.TabIndex = 12;
            this.RDB_DaiHan.TabStop = true;
            this.RDB_DaiHan.Text = "Dai Han";
            this.RDB_DaiHan.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(392, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 14);
            this.label7.TabIndex = 11;
            this.label7.Text = "Loai Lop";
            // 
            // TB_HocPhi
            // 
            this.TB_HocPhi.Location = new System.Drawing.Point(109, 193);
            this.TB_HocPhi.Name = "TB_HocPhi";
            this.TB_HocPhi.Size = new System.Drawing.Size(266, 20);
            this.TB_HocPhi.TabIndex = 10;
            // 
            // DTP_NgayKhaiGiang
            // 
            this.DTP_NgayKhaiGiang.Location = new System.Drawing.Point(109, 161);
            this.DTP_NgayKhaiGiang.Name = "DTP_NgayKhaiGiang";
            this.DTP_NgayKhaiGiang.Size = new System.Drawing.Size(266, 20);
            this.DTP_NgayKhaiGiang.TabIndex = 9;
            // 
            // CBB_TKB
            // 
            this.CBB_TKB.FormattingEnabled = true;
            this.CBB_TKB.Location = new System.Drawing.Point(109, 130);
            this.CBB_TKB.Name = "CBB_TKB";
            this.CBB_TKB.Size = new System.Drawing.Size(266, 22);
            this.CBB_TKB.TabIndex = 8;
            // 
            // TB_TenLopHOc
            // 
            this.TB_TenLopHOc.Location = new System.Drawing.Point(109, 98);
            this.TB_TenLopHOc.Name = "TB_TenLopHOc";
            this.TB_TenLopHOc.Size = new System.Drawing.Size(266, 20);
            this.TB_TenLopHOc.TabIndex = 7;
            // 
            // TB_MaLopHoc
            // 
            this.TB_MaLopHoc.Location = new System.Drawing.Point(109, 65);
            this.TB_MaLopHoc.Name = "TB_MaLopHoc";
            this.TB_MaLopHoc.Size = new System.Drawing.Size(266, 20);
            this.TB_MaLopHoc.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "Hoc Phi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 14);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ngay Khai Giang";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Thoi Khoa Bieu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ten Lop Hoc";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ma Lop Hoc";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(229, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "QUAN LY LOP HOC";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(2, 242);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1105, 338);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.DT_MonHoc);
            this.panel4.Location = new System.Drawing.Point(644, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(458, 365);
            this.panel4.TabIndex = 1;
            // 
            // DT_MonHoc
            // 
            this.DT_MonHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DT_MonHoc.Location = new System.Drawing.Point(3, 3);
            this.DT_MonHoc.Name = "DT_MonHoc";
            this.DT_MonHoc.Size = new System.Drawing.Size(458, 332);
            this.DT_MonHoc.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DT_LopHoc);
            this.panel3.Location = new System.Drawing.Point(0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(638, 362);
            this.panel3.TabIndex = 0;
            // 
            // DT_LopHoc
            // 
            this.DT_LopHoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DT_LopHoc.Location = new System.Drawing.Point(3, 3);
            this.DT_LopHoc.Name = "DT_LopHoc";
            this.DT_LopHoc.Size = new System.Drawing.Size(632, 329);
            this.DT_LopHoc.TabIndex = 0;
            // 
            // trungTamDunnoDataSetBindingSource
            // 
            this.trungTamDunnoDataSetBindingSource.DataSource = this.trungTamDunnoDataSet;
            this.trungTamDunnoDataSetBindingSource.Position = 0;
            // 
            // trungTamDunnoDataSet
            // 
            this.trungTamDunnoDataSet.DataSetName = "TrungTamDunnoDataSet";
            this.trungTamDunnoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // trungTamDunnoDataSetBindingSource1
            // 
            this.trungTamDunnoDataSetBindingSource1.DataSource = this.trungTamDunnoDataSet;
            this.trungTamDunnoDataSetBindingSource1.Position = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quanLyToolStripMenuItem,
            this.taiKhoanToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(638, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // quanLyToolStripMenuItem
            // 
            this.quanLyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sinhVienToolStripMenuItem,
            this.bienLaiToolStripMenuItem});
            this.quanLyToolStripMenuItem.Name = "quanLyToolStripMenuItem";
            this.quanLyToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.quanLyToolStripMenuItem.Text = "Quan Ly";
            // 
            // taiKhoanToolStripMenuItem
            // 
            this.taiKhoanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quanLyTaiKhoanToolStripMenuItem,
            this.dangXuatToolStripMenuItem});
            this.taiKhoanToolStripMenuItem.Name = "taiKhoanToolStripMenuItem";
            this.taiKhoanToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.taiKhoanToolStripMenuItem.Text = "Tai Khoan";
            this.taiKhoanToolStripMenuItem.Click += new System.EventHandler(this.taiKhoanToolStripMenuItem_Click);
            // 
            // sinhVienToolStripMenuItem
            // 
            this.sinhVienToolStripMenuItem.Name = "sinhVienToolStripMenuItem";
            this.sinhVienToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.sinhVienToolStripMenuItem.Text = "Sinh Vien";
            // 
            // bienLaiToolStripMenuItem
            // 
            this.bienLaiToolStripMenuItem.Name = "bienLaiToolStripMenuItem";
            this.bienLaiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.bienLaiToolStripMenuItem.Text = "Bien Lai";
            // 
            // quanLyTaiKhoanToolStripMenuItem
            // 
            this.quanLyTaiKhoanToolStripMenuItem.Name = "quanLyTaiKhoanToolStripMenuItem";
            this.quanLyTaiKhoanToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.quanLyTaiKhoanToolStripMenuItem.Text = "Quan Ly Tai Khoan";
            // 
            // dangXuatToolStripMenuItem
            // 
            this.dangXuatToolStripMenuItem.Name = "dangXuatToolStripMenuItem";
            this.dangXuatToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dangXuatToolStripMenuItem.Text = "Dang Xuat";
            // 
            // FLopHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 579);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FLopHoc";
            this.Text = "Quan Ly Lop Hoc";
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DT_MonHoc)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DT_LopHoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trungTamDunnoDataSetBindingSource1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton RDB_NganHan;
        private System.Windows.Forms.RadioButton RDB_DaiHan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TB_HocPhi;
        private System.Windows.Forms.DateTimePicker DTP_NgayKhaiGiang;
        private System.Windows.Forms.ComboBox CBB_TKB;
        private System.Windows.Forms.TextBox TB_TenLopHOc;
        private System.Windows.Forms.TextBox TB_MaLopHoc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.BindingSource trungTamDunnoDataSetBindingSource;
        private TrungTamDunnoDataSet trungTamDunnoDataSet;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BTN_Them;
        private System.Windows.Forms.BindingSource trungTamDunnoDataSetBindingSource1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DT_MonHoc;
        private System.Windows.Forms.DataGridView DT_LopHoc;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem quanLyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinhVienToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bienLaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taiKhoanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quanLyTaiKhoanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dangXuatToolStripMenuItem;
    }
}