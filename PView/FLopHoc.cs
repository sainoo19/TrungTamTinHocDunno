﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dunno.PControl;

namespace Dunno.PView
{
    public partial class FLopHoc : Form
    {
        ControlLopHoc ctrlLopHoc = new ControlLopHoc();
        ControlMonHoc ctrlMonHoc = new ControlMonHoc();
        List<LopHoc> listlophoc = new List<LopHoc>();
        LopHoc lophoc;

        public FLopHoc()
        {
            InitializeComponent();
            loadcbblophoc();
            listlophoc = ctrlLopHoc.FindAll();
            loaddslophoc();
            lophoc = listlophoc[0];
            //loadlophoc(lophoc);
            LoadDSMONHOC(lophoc.MonHocs.ToList());
        }

        void loaddslophoc()
        {
            var load = ctrlLopHoc.FindAll();
            var rs = from t in load select new { t.MaLopHoc, t.TenLopHoc ,t.LoaiLop,t.ThoiKhoaBieu,t.NgayKhaiGiang,t.HocPhi};
            DT_LopHoc.DataSource = rs.ToList();
        }
        /*void loadlophoc(LopHoc lh)
        {
            TB_MaLopHoc.Text = lh.MaLopHoc;
            TB_TenLopHOc.Text = lh.TenLopHoc;
            DTP_NgayKhaiGiang.Value = DateTime.Parse(lh.NgayKhaiGiang.ToString());
        }*/
        void LoadDSMONHOC(List<MonHoc> lstmh)
        {
            var rst = from t in lstmh
                      select new { t.MaLopHoc, t.TenMonHoc, t.SoTietLyThuyet, t.SoTietThucHanh};
            DT_MonHoc.DataSource = rst.ToList();
        }

        /*private void dtLopHoc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int idRow = DT_LopHoc.CurrentCell.RowIndex;
            lophoc = listlophoc[idRow];
            loadlophoc(lophoc);
            LoadDSMONHOC(lophoc.MonHocs.ToList());
        }*/

        void loadcbblophoc()
        {
            CBB_TKB.Items.Add("Thu 2-4-6");
            CBB_TKB.Items.Add("Thu 3-5-7");
        }

        private void BTN_Them_Click(object sender, EventArgs e)
        {
            LopHoc lh = new LopHoc
            {
                MaLopHoc = TB_MaLopHoc.Text,
                TenLopHoc = TB_TenLopHOc.Text,
                NgayKhaiGiang = DTP_NgayKhaiGiang.Value,
                HocPhi = int.Parse(TB_HocPhi.Text),
                LoaiLop = (RDB_DaiHan.Checked == true ? "Dai Han" : "Ngan Han"),
                ThoiKhoaBieu = CBB_TKB.SelectedItem.ToString(),
            };
            ctrlLopHoc.add(lh);
            loaddslophoc();
        }

        private void taiKhoanToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
