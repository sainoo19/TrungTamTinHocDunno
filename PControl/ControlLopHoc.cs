﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dunno.PControl
{
    internal class ControlLopHoc
    {
        TrungTamDunnoEntities db = UtilDB.qltt;

        public List<LopHoc> FindAll()
        {
            var dslophoc = from s in db.LopHocs select s;
            return dslophoc.ToList();
        }

        public void add(LopHoc s)
        {
            db.LopHocs.Add(s);
            db.SaveChanges();
        }
    }
}
