
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Dunno
{

using System;
    using System.Collections.Generic;
    
public partial class HocVien
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public HocVien()
    {

        this.DiemThis = new HashSet<DiemThi>();

        this.BienLais = new HashSet<BienLai>();

    }


    public string MaHocVien { get; set; }

    public string MaLopHoc { get; set; }

    public string HoTen { get; set; }

    public string GioiTinh { get; set; }

    public Nullable<System.DateTime> NgaySinh { get; set; }

    public string NoiSinh { get; set; }

    public string NgheNghiep { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<DiemThi> DiemThis { get; set; }

    public virtual LopHoc LopHoc { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<BienLai> BienLais { get; set; }

}

}
